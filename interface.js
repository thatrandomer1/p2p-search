var Server = require("./server.js");
var WebServer = require("./webserver");
server = new Server();
webserver = new WebServer();

var arguments = process.argv.splice(2);


my_port = Number(arguments[0]);
my_node_id = Number(arguments[1]);
my_ip_address = "127.0.0.0";



server.init(
	my_ip_address,
	my_port,
	my_node_id);


// for startup
if(arguments.length > 2)
{
	var port = arguments[2];
	console.log(port);
	var ip  = "127.0.0.1";
	server.joinNetwork(ip,port);
}



process.on( 'SIGINT', function() {
	console.log("\nleaving network");
  server.leaveNetwork();
  // wait for all the messages to be sent then quit
  setTimeout(process.exit,1000);
})


process.stdin.resume();
process.stdin.setEncoding('utf8');
process.stdin.on('data', function (d) {
	d = d.replace(/[\r\n]/g,'');
	var args = d.split(":");


 	if(args[0] == "i")
 	{
 		
 		process.stdout.write('index: ' + args[1] + ", link:" + args[2] + "\n");
 		server.index(args[1] , args[2]);
 	}else if(args[0] == "s"){
 		args.shift();
 		
 		process.stdout.write('searching: ' + JSON.stringify(args) + "\n");
 		server.search(args,function(links){
			console.log('search reply: ' + JSON.stringify(args) + ", link:" +JSON.stringify(links));
		}) ;

 	}else if(args[0] == "r"){
 		server.showRoutingTable();
 	}else if(args[0] == "rs"){
 		server.showLeafSetSmall();
 	}else if(args[0] == "rl"){
 		server.showLeafSetLarge();
 	}else if(args[0] == "l"){
 		server.showIndex();
 	}else if(args[0] == "m"){
 		server.showLastMessage();
 	}else if(args[0] == "p"){
 		server.ping_node(args[1]);
 	}else if(args[0] == "d"){
 		server.removeNode(args[1]);
 	}else if(args[0] == "q"){
 		console.log("quiting without sending leaving network messages");
 		server.badQuit();
 		process.exit();
 	}else if(args[0] == "w"){
 		webserver.start();
 	}

});





