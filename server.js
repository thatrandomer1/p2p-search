var dgram = require("dgram");
var Sender = require("./sender.js");
var RoutingTable = require("./routingTable.js");
var Indexer = require("./indexer.js");
var Acknowledger = require("./acknowledger.js");




module.exports = Server;


var server = dgram.createSocket("udp4");



function Server(){
	
}
 

Server.prototype.init = function(ip,port,id){
	 my_port = port;
	 my_node_id = id;
	 my_ip_address = ip;
	 my_node_info = makeNode(ip,port,id);
	 sender = new Sender(my_node_info);
	 routingTable = new RoutingTable(my_node_info);
	 indexer = new Indexer();
	 acknowledger = new Acknowledger();

	setupServer();
}


Server.prototype.leaveNetwork = function(){
  // dont accept anymore calls
  server.close();
  // some other closing procedures go here
  var d = routingTable.data()
  d.forEach( sender.send_leave_network );
}


Server.prototype.joinNetwork = function(ip,port){
	var node_info = new Object();
	node_info.port = port;
	node_info.ip_address = ip;
	sender.send_join_network(node_info);
}

Server.prototype.index = function(word,link)
{
	var hash = hashCode(word);
	var closest_node = routingTable.find_next(hash);

	if(closest_node.node_id == my_node_id){
		indexer.index(word,link);
		console.log("indexed");
	}
	else{

		sender.send_index(word,link,my_node_id,hash,closest_node);
		console.log("sending to " + closest_node.node_id);

		acknowledger.waitForIndexAck(word,function()
			{
				console.log("index ack not recieved - " + closest_node.node_id + " pinged ");
				// now we need to move onto the next node in the list
				ping_node(hash,closest_node);
			});

	}

}

Server.prototype.search = function(words,func)
{
	acknowledger.waitSearchs(func);

	words.forEach(function(word){
		console.log(word);
		var hash = hashCode(word);
		var closest_node = routingTable.find_next(hash);
		  
		if(closest_node.node_id == my_node_id){
			return acknowledger.setSearchLinks(indexer.search(word));
			//console.log("indexed");
		}
		else{


			acknowledger.waitForSearchAck(word,
				function(){
						console.log("search response not recieved - " + closest_node.node_id + " pinged ");
						// now we need to move onto the next node in the list
						ping_node(hash,closest_node);
				});

			sender.send_search(word,my_node_id,hash,closest_node);
			console.log("sending search request");
		}
	});

}

Server.prototype.showRoutingTable = function()
{
	console.log(routingTable.data());

}

Server.prototype.showIndex = function()
{
	console.log(indexer.data());

}

Server.prototype.showLastMessage = function()
{
	console.log(lastMessage);

}

Server.prototype.showLeafSetLarge = function()
{
	console.log(routingTable.LeafLarge());

}
Server.prototype.showLeafSetSmall = function()
{
	console.log(routingTable.LeafSmall());
}

Server.prototype.removeNode = function(id)
{
	routingTable.remove(id);
}

// quit without shutting down properly
Server.prototype.badQuit = function(id)
{
	  // dont accept anymore calls
  server.close();
}

Server.prototype.ping_node = function(id){
	ping_node(id,my_node_id);
}









function hashCode(str){
	return (str.split("").reduce(function(previousValue, currentValue, index, array){
		return (previousValue*31 + currentValue.charCodeAt(0))%(Math.pow(2,31)-1);
	},0));
}




function makeNode(ip,port,id){
	node =  new Object();
	node.ip_address = ip;
	node.node_id = id;
	node.port = port;
	return node;	
}


lastMessage = "";

function setupServer(){
	server.on("error", function (err) {
	  console.log("server error:\n" + err.stack);
	  server.close();
	});

	server.on("message", function (msg, rinfo) {
		lastMessage = JSON.parse(msg);

		var obj = JSON.parse(msg);
		var typ = obj.type;
		delete obj.type;

		switch(typ){
			case "JOINING_NETWORK":
				obj.node_id = Number(obj.node_id);
				JOINING_NETWORK(obj);
				break;
			case "JOINING_NETWORK_RELAY":
				obj.node_id = Number(obj.node_id);
				joining_network_relay(obj);
				break;
			case "ROUTING_INFO":
				routing_info(obj);
				break;
			case "LEAVING_NETWORK":
				leave_network(obj);
				break;
			case "INDEX":
				index_msg(obj);
				break;
			case "SEARCH":
				search_msg(obj);
				break;
			case "SEARCH_RESPONSE":
				search_response_msg(obj);
				break;
			case "PING":
				ping_msg(obj);
				break;
			case "ACK":
				ack_msg(obj);
				break;
			case "ACK_INDEX":
				ack_index(obj);
				break;
			default: 
				console.log("error msg:" + msg);
				break;
		}
	 
	});

	server.on("listening", function () {
	  var address = server.address();
	  console.log("server listening " +
	      address.address + ":" + address.port);
	});

	server.bind(my_port);
}



///// sort of server function

function ping_node(target_id,sender_id)
{
	id = Number(target_id);
	//console.log(routingTable.LeafSmall());
	var closest_node = routingTable.find_next(id);

	// this will likely happen if route changes
	if(closest_node.node_id == my_node_id){
		return;
	}

	acknowledger.waitForAck(id,closest_node.ip_address,closest_node.port,function()
		{
			console.log("ack not recieved - " + closest_node.node_id + " removed");
			routingTable.remove(closest_node.node_id);
			// now we need to move onto the next node in the list
			ping_node(id,sender_id);
		});

	sender.send_ping(id,sender_id,closest_node);
}



//////////////////////////////////////////////// server functions

function ack_msg(msg){
	console.log("recieved ack");
	acknowledger.recvAck(msg.node_id,msg.ip_address,msg.port);
}

function ack_index(msg){

	var closest_node = routingTable.find_next(msg.sender_id);

	if(closest_node.node_id == my_node_id){
		console.log("index ack recieved");
		acknowledger.recvIndexAck(msg.keyword);
	}else{
		sender.send_ack_index(msg.node_id,msg.keyword,closest_node);
	}

}


function ping_msg(msg){
	sender.send_ack(msg.target_id,msg.ip_address,msg.port);
	ping_node(msg.target_id,msg.sender_id);
}



function search_response_msg(msg){

	var closest_node = routingTable.find_next(msg.sender_id);

	if(closest_node.node_id == my_node_id){

		acknowledger.recvSearchAck(msg.word,msg.response);

		console.log("search returned");
		//console.log(msg.response);
	}
	else{

		sender.send_search_response(
			msg.word,
			msg.response,
			msg.sender_id,
			msg.node_id ,
			closest_node);


		console.log("sending to " + closest_node.node_id);
	}
}

function search_msg(msg){

	var closest_node = routingTable.find_next(msg.node_id);

	if(closest_node.node_id == my_node_id){
		console.log("search");
		var response = indexer.search(msg.word);

		var sender_closest_node = routingTable.find_next(msg.sender_id);

		sender.send_search_response(
			msg.word,
			response,
			msg.sender_id,
			msg.node_id ,
			sender_closest_node);
	}
	else{

		sender.send_search(
			msg.word,
			msg.sender_id,
			msg.node_id,
			closest_node);

		console.log("sending to " + closest_node.node_id);
	}
}

function index_msg(msg){
	console.log(msg.target_id);

	var closest_node = routingTable.find_next(msg.target_id);


	console.log(closest_node);

	if(closest_node.node_id == my_node_id){
		indexer.index(msg.keyword,msg.link);
		console.log("indexed");

		var sender_node = routingTable.find_next(msg.sender_id);
		sender.send_ack_index(msg.sender_id,msg.keyword,sender_node);
	}
	else{

		sender.send_index(
			msg.keyword,
			msg.link,
			msg.sender_id,
			msg.target_id,
			closest_node);

		console.log("sending to " + closest_node.node_id);
	}
}




function JOINING_NETWORK(node_info){
	
	var closest_node = routingTable.find_next(node_info.node_id);
	routingTable.add(node_info);
	

	// check we are the closest node just send routing table back
	if(closest_node.node_id == my_node_id){
			sender.send_routing_table(
				my_node_id,
				node_info.node_id,
				routingTable.data(),
				node_info.port);		
	}else // else send it to the closer node
	{
			sender.send_join_relay_network(
				my_node_id,
				node_info,
				closest_node);
	}


	//console.log(node_info);
}






function joining_network_relay(msg){
	var target_id = msg.node_id;

	// look for the target node
	var closest_node = routingTable.find_next(target_id);

	var new_node = makeNode(msg.ip_address,msg.port,msg.node_id);
	routingTable.add(new_node);
	
	if(closest_node.node_id == my_node_id){
			// find the gateway node to respond back
			var closest_gateway_node = routingTable.find_next(msg.gateway_id);

			sender.send_routing_table(
				msg.gateway_id,
				target_id,
				routingTable.data(),
				closest_gateway_node.port);
	}else{
			// if its not me send onto the next node
			sender.send_join_relay_network(
				msg.gateway_id,
				new_node,
				closest_node);
	}
}







function routing_info(route_info){

	route_info.route_table.forEach( function(node_info) {
    	routingTable.add(node_info);
	});

	// if the gateway node send table back to original node
	if(route_info.gateway_id == my_node_id)
	{
		var targat_node = routingTable.find_next(route_info.node_id);

		if(targat_node.node_id != route_info.node_id)
		{
			console.log("failed to target id in cache");
			return;
		}
		
		sender.send_routing_table(
			route_info.gateway_id,
			route_info.node_id,
			route_info.route_table,
			targat_node.port);

		
		//console.log("gateway");
		//console.log(route_info.route_table);
	}
	else if(route_info.node_id == my_node_id)
	{

		var gate_way_node = makeNode(
				route_info.ip_address,
				Number(route_info.port),
				route_info.gateway_id
			);

		routingTable.add(gate_way_node);


		//console.log("target :");
		//console.log(route_info.route_table);
	}
	else
	{
		var targat_node = routingTable.find_next(route_info.gateway_id);

		sender.send_routing_table(route_info.gateway_id,route_info.node_id,route_info.route_table,targat_node.port);
		//console.log("in transit :");
		//console.log(routingTable.data());
	}


}


function leave_network(node){
	//console.log("node left:" + JSON.stringify(node));
	routingTable.remove(node.node_id);
}



