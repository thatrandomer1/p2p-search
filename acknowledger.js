
module.exports = Acknowledger;

function Acknowledger(){
}

pending = {}



Acknowledger.prototype.waitForAck = function(id,ip,port,func){
	pending[id] = setTimeout(func,10000);
}

Acknowledger.prototype.recvAck = function(id,ip,port){
	clearTimeout(pending[id]);
}


indexPending = {}
Acknowledger.prototype.waitForIndexAck = function(word,func){
	indexPending[word] = setTimeout(func,10000);
}

Acknowledger.prototype.recvIndexAck = function(word){
	clearTimeout(indexPending[word]);
}




// these are used to tell if we need to send an ack a
searchPending = {};
// this is see if we should add the value to search response
searchPendingResponse = {};

// only one multisearch allowed at a time, user must wait at least 3 seconds before another search 
searchResponse = {};

Acknowledger.prototype.waitSearchs= function(returnFunc){
	setTimeout(function(){
		returnFunc(searchResponse);
		searchResponse = {};
	},3000);
}



Acknowledger.prototype.waitForSearchAck = function(word,failFunc){
	searchPending[word] = setTimeout(failFunc,30000);
	searchPendingResponse[word] = true;
	setTimeout(function(){searchPendingResponse[word]=null;},3000);
}

Acknowledger.prototype.recvSearchAck = function(word,links){

	if(searchPendingResponse[word]){
		for(var key in links){
			// something silly with javascript
			if (!links.hasOwnProperty(key)) {
	        	//The current property is not a direct property of p
	        	continue;
	    	}

		
			if(searchResponse[key])
				searchResponse[key] += links[key];
			else
				searchResponse[key] = links[key]

		}
	}
	clearTimeout(searchPending[word]);
}

Acknowledger.prototype.setSearchLinks = function(links){
		for(var key in links){
			// something silly with javascript
			if (!links.hasOwnProperty(key)) {
	        	//The current property is not a direct property of p
	        	continue;
	    	}

		
			if(searchResponse[key])
				searchResponse[key] += links[key];
			else
				searchResponse[key] = links[key]

		}
}







