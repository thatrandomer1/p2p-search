routing_table = [];
leaf_set_len = 2;





module.exports = RoutingTable;

function RoutingTable(my_node_info){
	this.my_node_info = my_node_info;
	this.my_node_id = my_node_info.id;

	// add my own nodes
	//add_node_to_leaf_set(my_node_info);
	
	close_small = [my_node_info];
	close_large = [my_node_info];
	add_node_routing_table(my_node_info);
}



RoutingTable.prototype.find_next = find_next_node;
RoutingTable.prototype.add = add_node;
RoutingTable.prototype.data = function(){
      return flatten(routing_table);
}

RoutingTable.prototype.LeafLarge = function(){
      return close_large;
}

RoutingTable.prototype.LeafSmall = function(){
      return close_small;
}


RoutingTable.prototype.remove = function(node_id){
	remove_from_leaf_set(node_id);
	remove_from_routing_table(node_id);
}


function find_next_node(node_id){
	var closest_node;
	
	if(in_leaf_set(node_id)){
		 return getClosestLeaf(node_id);
	}else{
		
		var pos = GetPrefixMatch(node_id);
		var prefixByte = getPrefixByte(node_id,pos);
		

		if(routing_table[pos] && routing_table[pos][prefixByte]){
			return  routing_table[pos][prefixByte];
		}else{
			for(var i = pos;i <= 6;i++){
				if(routing_table[i] && routing_table[i].length > 0){
					var ans = getClosestPrefix(routing_table[i],prefixByte,node_id);
					if(ans){
						return ans;
					}
				}
			}
		}
	}
	
	// I think this is bad
	console.log("something bad happened in find_next_node");
	return my_node_info;
}

function add_node(node_info){
	// no need to add my self
	if(node_info.node_id == my_node_id) return;
	add_node_routing_table(node_info);
	add_node_to_leaf_set(node_info);
}




// private 




///////////////////////////////// Leaf set functions

function addClose(node_info,array,op){
	if(node_info.node_id == my_node_id)
		return array;

	var prev = node_info; 

	for(var i=0;i<array.length;i++)
	{ 
		if(prev.node_id == array[i].node_id)
		{
			// this means the node_id is already in the array
			return array;
		}
		else if(op(prev.node_id,array[i].node_id))
		{
			tmp = prev;
			prev = array[i];
			array[i] = tmp; 
		}
	}

	if(array.length < leaf_set_len)
		array.push(prev);

	return array;
}

function add_node_to_leaf_set(node_info){
	if(node_info.node_id < my_node_id){
		close_small = addClose(node_info ,close_small,gt);
	}
	else{
		close_large = addClose(node_info ,close_large,lt);
	}
}

function remove_from_leaf_set(node_id){
	var  removeClose = function(set){
		for(var i=0;i<set.length;i++)
		{ 
			if(set[i].node_id == node_id){
				set.splice(i,1);
				return;
			}
		}
	}

	if(node_id < my_node_id){
		removeClose(close_small);
	}
	else{
		removeClose(close_large);
	}
}

function in_leaf_set(id){

	var isInSet = function(set,op){
		// if the array is larger than the leaf set 
		// then check it against all of elements in the set
		if(set.length >= leaf_set_len)
		{	
			return  op(id ,set[set.length - 1].node_id);
		}else{
			return true;
		}
	};

	if(id > my_node_id)
		return isInSet(close_large,lt);
	else 
		return isInSet(close_small,gt);
}



// Get closest in leaf set
function getClosestLeaf(num)
{
	var list = (close_small.slice(0).reverse());

	list = list.concat([my_node_info],close_large);
		

	return list.reduce(function (prev, curr) {
  		return (Math.abs(curr.node_id - num) < Math.abs(prev.node_id - num) ? curr : prev);
	});


}

///////////////////////////////////////////////////////////////////routing table functions


function remove_from_routing_table(node_id){
	var pos = GetPrefixMatch(node_id);

	var route = routing_table[pos];
	if(!route){
		// it wasn't in the routing table (this may indicate its broken)
		return;
	}

	var prefixByte = getPrefixByte(node_id,pos);
	if(route[prefixByte] && route[prefixByte].node_id == node_id)
		delete route[prefixByte];

	routing_table[pos] = route;
}


function add_node_routing_table(node_info){
	var pos = GetPrefixMatch(node_info.node_id);

	var route = routing_table[pos];
	if(!route){
		route = [];
		//route[15] = undefined;
	}
	var prefixByte = getPrefixByte(node_info.node_id,pos);
	route[prefixByte] = node_info;

	routing_table[pos] = route;
}



function getClosestPrefix(list,num,num_id){
	for(var i = 0;i<16;i++)
	{
		var node = list[i];
		
		if(node){
			var val = node.node_id;
			if(Math.abs(val - num_id) < Math.abs(my_node_id - num_id)){
				return node;
			}
		}
		
	}
	return false;
}




///////////// utitly functions

function gt(a,b){return a>=b}
function lt(a,b){return a<=b}


function GetPrefixMatch(id)
{
	var bitPattern = (my_node_id^id);
	var match = 0;
	for(var i = 6; i >= 0;i--)
	{
		if(!(bitPattern&(15<<(i*4))))
			match+=1;
		else 
			return match;
	}
	// this should never be called, it means id and node_id are the same
	return match;
}

function getPrefixByte(id,pos){
	var bitPattern = id;
	bitPos =  (4*(5-pos));
	return (id&15<<bitPos)>>bitPos;
}





function flatten(arr){
	if(arr.length ==0)return [];

	return arr.reduce(function(a, b){
     return a.concat(b);
	}).filter(function(n){return n}); // get rid of elements that don't exist
}

