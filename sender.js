var dgram = require("dgram");

module.exports = Sender;

function Sender(my_node_info){
	this.my_node_info = my_node_info;
}

Sender.prototype.send_routing_table  = function(gateway_id,node_id,route_table,po){
    var msg = new Object();
    msg.type="ROUTING_INFO";
    msg.gateway_id = gateway_id;
    msg.node_id = node_id;
    msg.ip_address = my_ip_address;
    msg.port = my_port;
    msg.route_table = route_table;


    sendMessage(po,msg);
}

Sender.prototype.send_join_network = function(node_info){
	var msg = new Object();

	msg.type ="JOINING_NETWORK";
	msg.node_id = my_node_info.node_id;
	msg.ip_address = my_node_info.ip_address;
    msg.port = my_node_info.port;

    sendMessage(node_info.port,msg);
}

Sender.prototype.send_join_relay_network = function(gateway_id,new_node,send_node_info){
	var msg = new Object();
	msg.type ="JOINING_NETWORK_RELAY";
	msg.gateway_id = gateway_id;
	msg.ip_address = new_node.ip_address;
    msg.port = new_node.port;
	msg.node_id = new_node.node_id;
    sendMessage(send_node_info.port,msg);
}


Sender.prototype.send_leave_network = function(send_node_info){
	// never send leave message to my own port
	if(send_node_info.port == my_node_info.port)return;

	var msg = new Object();
	msg.type ="LEAVING_NETWORK";
	msg.node_id = my_node_info.node_id;
    sendMessage(send_node_info.port,msg);
    console.log(send_node_info);
}

Sender.prototype.send_index = function(keyword,links,sender_id,target_id,send_node_info){
	// never send leave message to my own port
	if(send_node_info.port == my_node_info.port)return;

	var msg = new Object();
	msg.type ="INDEX";
	msg.target_id = target_id;
	msg.sender_id = sender_id;
	msg.keyword = keyword;
	msg.link = links;

    sendMessage(send_node_info.port,msg);
    //console.log(send_node_info);
}


Sender.prototype.send_search = function(word,sender_id,target_id,send_node_info){
	// never send leave message to my own port
	if(send_node_info.port == my_node_info.port)return;

	var msg = new Object();
	msg.type ="SEARCH";
	msg.node_id = target_id;
	msg.sender_id = sender_id;
	msg.word = word;

    sendMessage(send_node_info.port,msg);
    //console.log(send_node_info);
}


Sender.prototype.send_search_response = function(word,response,sender_id,target_id,send_node_info){
	// never send leave message to my own port
	if(send_node_info.port == my_node_info.port)return;

	var msg = new Object();
	msg.type ="SEARCH_RESPONSE";
	msg.word = word;
	msg.node_id = target_id;
	msg.sender_id = sender_id;
	msg.response = response;


    sendMessage(send_node_info.port,msg);
    //console.log(send_node_info);
}


Sender.prototype.send_ping = function(target_id,sender_id,send_node_info){

	var msg = new Object();
	msg.type ="PING";
	msg.target_id = target_id;
	msg.sender_id = sender_id;

	msg.ip_address = my_ip_address;
	msg.port = my_port;

    sendMessage(send_node_info.port,msg);
}


Sender.prototype.send_ack = function(node_id,ip_address,port){

	var msg = new Object();
	msg.type ="ACK";
	msg.node_id = node_id;
	msg.ip_address = my_ip_address;
	msg.port = my_port;

    sendMessage(port,msg);
}

Sender.prototype.send_ack_index = function(node_id,keyword,send_node_info){

	var msg = new Object();
	msg.type ="ACK_INDEX";
	msg.node_id = node_id;
	msg.keyword = keyword;

    sendMessage(send_node_info.port,msg);
}




/////  Private 
function sendMessage(port,obj)
{
	// dont send message to myself
	if(my_node_info.port==port){console.log("sending messages to myself something broke");return;};

	var client = dgram.createSocket("udp4");
	var message = new Buffer(JSON.stringify(obj));
	//console.log(node_info);
	client.send(message, 0, message.length,Number(port), "localhost", function(err, bytes) {
	  client.close();
	});
}

